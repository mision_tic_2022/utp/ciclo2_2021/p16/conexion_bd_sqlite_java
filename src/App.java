import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        
        Connection conn;
        //Generar conexion a la base de datos
        conn = DriverManager.getConnection("jdbc:sqlite:hr.db");
        //
        Statement statement = conn.createStatement();
        statement.setQueryTimeout(30);

        ResultSet employees = statement.executeQuery("SELECT * FROM employees ORDER BY salary DESC");
        while(employees.next()){
            System.out.println("id-> "+employees.getInt("employee_id"));
            System.out.println("firstname-> "+employees.getString("first_name"));

        }

    }
}

